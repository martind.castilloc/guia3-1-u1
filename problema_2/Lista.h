#ifndef LISTA_H
#define LISTA_H

#include <iostream>

using namespace std;

//ESTRUCTURA DEL NODO
typedef struct _Nodo {
    string words;
    struct _Nodo *sig;
} Nodo;

class  Lista{

    private:
        Nodo *first_n = NULL;

    public:
        Lista();

        //Funcion que crea lista.
        void agregar(string word);
        //Funcion que imprime la lista.
        void mostrar();
        //Funcion para limpiar la terminal.
        void clear();
};
#endif