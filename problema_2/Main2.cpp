#include <iostream>

#include "Lista.h"

using namespace std;

void crea_lista(Lista *lista){
    int opc;
    int i=0;
    string palabra;

    do{
        cout << "\n";
        cout << "\t     ---------------------------- " << endl;
		cout << "\t     ***** MENÚ DE OPCIONES ***** " << endl;
		cout << "\t     ---------------------------- " << endl;
		cout << "\n  [1]  > Ingresar una palabra\t" << endl;
		cout << "  [2]  > Salir del programa\t" << endl;

        cout << "\nOpción: ";
        cin >> opc;
		cin.ignore();

        if (opc == 1){
            lista->clear();
            cout << "\n";
            cout << "Ingrese la palabra: ";
			cin >> palabra;
			cout << endl;
			lista->agregar(palabra);
            lista->mostrar();
		}
        
        else if(opc == 2){
            lista->clear();
            cout << "\n\t¡Se ha cerrado el programa correctamente!\n" << endl;
            // Se libera memoria del sistema.
            delete lista;
			break;
        }

        else{
            cout << "\t--------------------------------------" << endl;
			cout << "\tOpción Invalida. " << endl;
			cout << "\t--------------------------------------\n" << endl;
        }
    }while(i<1);
}

int main(void){
    // Se crea el objeto Lista.
    Lista *lista = new Lista();
    crea_lista(lista);

    return 0;
}
