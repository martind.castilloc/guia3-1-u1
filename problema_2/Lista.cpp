#include <iostream>

#include "Lista.h"

using namespace std;

Lista::Lista(){}

//funcion que permite agregar las palabras.
void Lista::agregar(string word){

    //Se crea un nodo.
    Nodo *nod = new Nodo();
    nod->words = word;
    nod->sig = NULL;

    if(this->first_n == NULL){
        this->first_n = nod;
    }else{
        Nodo *second, *nuevo;
        second = this->first_n;

        while (second !=NULL && second->words < word){
            nuevo = second;
            second = second->sig;
        }

        if (second == this->first_n){
            this->first_n = nod;
            this->first_n->sig = second;
        } else{
            nuevo->sig = nod;
            nod->sig = second;
        }  
    }
}

//Función para limpiar la terminal, no afecta en el problema.
void Lista::clear() {
    cout << "\x1B[2J\x1B[H";
}

// Funcion que imprime el contenido de las listas, usando un nodo.
void Lista::mostrar(){
    //Utiliza variable temporal para recorrer la lista.
    Nodo *nod = this->first_n;

    while (nod != NULL){
        cout<< nod->words <<endl;
        nod = nod->sig;
    }
    cout<<endl;
}

