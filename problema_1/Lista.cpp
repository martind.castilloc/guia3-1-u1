#include <iostream>

#include "Lista.h"

using namespace std;

Lista::Lista(){}

void Lista::agregar(int num){

    //Se crea un nodo.
    Nodo *nod = new Nodo();
    nod->numero = num;
    nod->sig = NULL;

    if(this->first_n == NULL){
        this->first_n = nod;
    }else{
        Nodo *second, *nuevo;
        second = this->first_n;

        while (second !=NULL && second->numero < num){
            nuevo = second;
            second = second->sig;
        }

        if (second == this->first_n){
            this->first_n = nod;
            this->first_n->sig = second;
        } else{
            nuevo->sig = nod;
            nod->sig = second;
        }  
    }
}

//Función para limpiar la terminal, no afecta en el problema.
void Lista::clear() {
    cout << "\x1B[2J\x1B[H";
}

// Funcion que imprime el contenido de las listas, usando un nodo
void Lista::mostrar(){
    //Utiliza variable temporal para recorrer la lista.
    Nodo *nod = this->first_n;

    while (nod != NULL){
        cout<< nod->numero <<endl;
        nod = nod->sig;
    }
    cout<<endl;
}

/*Funcion que nos permirte separar los numeros positivos de los negativos,
  ademas de mostarlos en listas independientes.
*/
void Lista::separar(Lista *lista){

    Lista *lp = new Lista();
    Lista *ln = new Lista();

    Nodo *r = lista->first_n;

    while (r != NULL){

        if(r->numero >= 0){
            lp->agregar(r->numero);
        }else{
            ln->agregar(r->numero);
        }

        r = r->sig; 
    }  
    
    cout << endl << "Lista positiva: " << endl;
	lp->mostrar();
	cout << "Lista negativa: " << endl;
	ln->mostrar();
}