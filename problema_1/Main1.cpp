#include <iostream>

#include "Lista.h"

using namespace std;

void crea_lista(Lista *lista){
    int can,opc;
    int i=0;

    do{
        //
        cout << "\n";
        cout << "\t     ---------------------------- " << endl;
		cout << "\t     ***** MENÚ DE OPCIONES ***** " << endl;
		cout << "\t     ---------------------------- " << endl;
		cout << "\n  [1]  > Ingresar un numero\t" << endl;
		cout << "  [2]  > Mostrar Listas separadas\t" << endl;
        cout << "  [3]  > Salir del programa\t" << endl;

        cout << "\nOpción: ";
        cin >> opc;
		cin.ignore();

        if (opc == 1){
            lista->clear();
            cout << "\n";
            cout << "Ingrese el numero: ";
			cin >> can;
			cout << endl;
			lista->agregar(can);
		}
        
        else if(opc == 2){
            lista->clear();
            lista->separar(lista);
        }

        else if(opc == 3){
            lista->clear();
            cout << "\n\t¡Se ha cerrado el programa correctamente!\n" << endl;
			break;
        }

        else{
            cout << "\t--------------------------------------" << endl;
			cout << "\tOpción Invalida. " << endl;
			cout << "\t--------------------------------------\n" << endl;
        }
    }while(i<1);
}

int main(void){
    // Se crea el objeto Lista.
    Lista *lista = new Lista();
    crea_lista(lista);

    return 0;
}